<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Raíz: login
Route::get('/', 'LoginController@index')->name('login');
//Ingreso al sistema
Route::post('login/','LoginController@login');
//Registrando 
Route::get('register/', 'LoginController@register')->name('register');
//Rutas para almacenamiento de usuairo nuevo
Route::post('register/','LoginController@store_register');
//Salir
Route::get('logout/','LoginController@logout');


//Home
Route::middleware('auth')->group(function() {
    Route::get('home/',function(){
        return view('main.home');
    });

});
