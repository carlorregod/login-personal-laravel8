<!doctype html>
<html lang="es">
<head>
    <title>Registro</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
    {!! $errors->first('credencial','<div class="alert alert-danger">:message</div>') !!}
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-5">
                    <h2 class="heading-section">Bienvenido</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-7 col-lg-5">
                    <div class="login-wrap p-4 p-md-5">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-wpforms"></span>
                        </div>
                        <h3 class="text-center mb-4">Regístrese</h3>
                        <form method="POST" action="{{url('register')}}" class="login-form">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control rounded-left" placeholder="Nombre y apellido de usuario" value="{{old('name')}}" required">
                                {!! $errors->first('name','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control rounded-left" placeholder="Número de teléfono" value="{{old('phone')}}" required>
                                {!! $errors->first('phone','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control rounded-left" placeholder="Correo electrónico" value="{{old('email')}}" required>
                                {!! $errors->first('email','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <input type="text" name="user" class="form-control rounded-left" placeholder="Usuario ID" value="{{old('user')}}" required>
                                {!! $errors->first('user','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control rounded-left" placeholder="Contraaseña">
                                {!! $errors->first('password','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" name="password_confirmation" class="form-control rounded-left" placeholder="Confirme Contraaseña">
                                {!! $errors->first('password_confirmation','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <button type="submit"
                                    class="form-control btn btn-primary rounded submit px-3">Registrarse</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50">
                                    <a href="{{route('login')}}">Ingresar al sistema</a>
                                </div>
                                <div class="w-50 text-md-right">
                                    {{--  --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/jquery.min.js"></script>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/popper.js"></script>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/bootstrap.min.js"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>
