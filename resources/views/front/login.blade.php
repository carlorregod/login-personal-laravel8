<!doctype html>
<html lang="es">
<head>
    <title>Ingreso</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
   {{--  @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
    {{-- <pre>
    {{var_dump(auth()->user())}}
    </pre> --}}
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-5">
                    <h2 class="heading-section">Bienvenido</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-7 col-lg-5">
                    <div class="login-wrap p-4 p-md-5">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-user-o"></span>
                        </div>
                        <h3 class="text-center mb-4">Ingrese al sistema</h3>
                        <form method="POST" action="{{url('login/')}}" class="login-form">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="user" class="form-control rounded-left" placeholder="Nombre de usuario" required>
                                {!! $errors->first('user','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control rounded-left" placeholder="Contraaseña" required>
                                    {!! $errors->first('password','<small>:message</small><br>') !!}
                            </div>
                            <div class="form-group">
                                <button type="submit"
                                    class="form-control btn btn-primary rounded submit px-3">Ingresar</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50">
                                    <label class="checkbox-wrap checkbox-primary">Recordarme
                                        <input type="checkbox" name="remember" checked>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="w-50 text-md-right">
                                    <a href="#">¿Olvidó contraseña?</a>
                                </div>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50">
                                    <a href="{{url('register')}}">Registrarse</a>
                                </div>
                                <div class="w-50 text-md-right">
                                    {{--  --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/jquery.min.js"></script>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/popper.js"></script>
    <script src="https://preview.colorlib.com/theme/bootstrap/login-form-11/js/bootstrap.min.js"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>
