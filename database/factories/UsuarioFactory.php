<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Usuario;
use Faker\Generator as Faker;

$factory->define(Usuario::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user' => $faker->unique()->userName,
        'phone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'rol' => 1,
        'password' => bcrypt('holamundo15'), // password
        'remember_token' => Str::random(10),
    ];
});
