<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Modleo usuario
use App\Usuario;


class LoginController extends Controller
{
    // Evitar login fallados repetidas veces
    protected $maxAttempts = 2; // De manera predeterminada sería 5
    protected $decayMinutes = 1; // De manera predeterminada sería 1
    protected $flagAttemps = true; //true: limitará número de intentos, false: intentos ilimitados

    //Confección del usernam,e clave
    public static function makeuserkey($username, $ip) {
        return $username.'@'.$ip;
    }


/* ---------------------------- Inicio de sesión ---------------------------- */
    public function index()
    {
        if(auth()->user() != NULL) {
            return redirect('/home');
        }
        return view('front.login');
    }

/* --------------------------- Registrando usuario -------------------------- */
    public function register()
    {
        return view('front.register');
    }

/* ------------------- Almacenando un nuevo usuario en BD ------------------- */
    public function store_register() 
    {
        request()->validate([
            'name'=>'required|min:3',
            'phone'=>'required|regex:/^[+]?[1-9]*[0-9]+$/i',
            'email'=>'required|email',
            'user'=>'required|min:3|max:15',
            'password'=>'required|alpha_num|min:7|confirmed',
        ]);
       //dd(auth()->attempt(['user'=>request()->user]));

        //Pasa validación, se crea nuevo usuario
        try{
            Usuario::insert([
                'name' => request()->name,
                'phone' => request()->phone,
                'email' => request()->email,
                'user' => request()->user,
                'password' => bcrypt(request()->password),
                'rol' => 1
            ]);

        } catch(\Exception $e) {
            return redirect()->back()->withErrors(['credencial'=>'El usuario ya existe en el sistema, favor elegir otro']);
        }

        //Además redirigirá al main como si fuese el login de laravel! 
        if(auth()->attempt(['user'=>request()->user, 'password'=>request()->password])){

    	    return redirect('/home');
        }
        
        return redirect()->back()->withErrors(['credencial'=>'Error al registrar usuario. favor reintentar']);
    }

    public function login() {  
         //Revisando contador de intentos login
        // $fecha_ahora = 'Carbon\Carbon'::now();
        $ip = request()->ip();
        $username = request()->user;

        $makeuserkey = self::makeuserkey($username, $ip);

        if($this->flagAttemps) {
            if(cache()->has($makeuserkey.':ban')) {
                return redirect()->back()->withErrors(['user'=>'Haz intentado acceder al sistema reiteradas veces. Favor reintentarlo dentro de unos minutos']);
            } 

        }
        //Validación inicial    
        request()->validate([
            'user'=>'required|string|min:3|max:15',
            'password'=>'required|alpha_num|min:7',
        ]);      

        if(request()->remember == null)
            $checked = false;
        else
            $checked= true;
        if(auth()->attempt(['user'=>request()->user, 'password'=>request()->password], $checked)){
            if($this->flagAttemps) {
                cache()->forget($makeuserkey);
                cache()->forget($makeuserkey.':ban');
            }
    	    return redirect('/home');
        } 

        if($this->flagAttemps) {
            if(!cache()->has($makeuserkey))
                cache()->forever($makeuserkey,0);

            #Incrementando en 1 el contador de intentos
            cache()->increment($makeuserkey, 1);
            # ¿Se sobrepasaron los intentos permitidos?
            if(cache()->get($makeuserkey) >= $this->maxAttempts) {
                cache()->put($makeuserkey.':ban', 1,$this->decayMinutes*60); //Los toma como segundos...
                cache()->forget($makeuserkey);
                return redirect()->back()->withErrors(['user'=>'Haz intentado acceder al sistema reiteradas veces. Favor reintentarlo dentro de unos minutos']);
            }


        } 
        return redirect()->back()->withErrors(['user'=>'Usuario y/o contraseñas no coinciden']);
    }

    public function logout()
    {
    	//Salida del autentificador y retorno al login
    	auth()->logout();
    	return redirect('/');

    }


  
}
