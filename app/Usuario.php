<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Uses de la clase
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
//Para hashear la pw
use Illuminate\Support\Facades\Hash;
//Incluir para el login
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Usuario extends Model implements AuthenticatableContract
{
    
    use Notifiable, Authenticatable;

    protected $fillable = [
        'name', 'user', 'phone', 'email', 'password', 'rol', 'traits_login'
    ];

    protected $primaryKey= 'id';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_login' => 'datetime',
    ];

    //Constructor de pw encriptados
	public function setPasswordAttribute($pass)
	{
    	$this->attributes['password'] = Hash::make($pass);
	}

}
